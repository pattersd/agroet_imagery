import os


date_fmt = "%Y-%m-%d"


class Utils(object):
    def get_band_path(self, band, scene_id):
        band_path = os.path.join(
            self._imagery_cache_dir, "{0}_{1}.TIF".format(scene_id, band)
        )
        return band_path
