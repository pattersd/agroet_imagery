import datetime, unittest
from main import Imagery
from utils import date_fmt


class Tests(unittest.TestCase):
    def test_get_products(self):
        imagery = Imagery()
        start_date = datetime.datetime.strptime("2019-06-13", date_fmt)
        end_date = datetime.datetime.strptime("2019-07-13", date_fmt)
        centroid = [-117.6414, 34.2708]
        prods = imagery.get_products(centroid, start_date, end_date)
        self.assertTrue(len(prods) > 1)

    def test_get_products_row_path(self):
        imagery = Imagery()
        start_date = datetime.datetime.strptime("2019-06-13", date_fmt)
        end_date = datetime.datetime.strptime("2019-07-13", date_fmt)
        prods = imagery.get_products_row_path(37, 42, start_date, end_date)
        self.assertGreater(len(prods), 0)

    # def test_get_scene(self):
    #     imagery = Imagery()
    #     scene_id = "LC80420372019173LGN00"
    #     bqa = imagery.get_band(scene_id, "BQA")
    #     proj4 = bqa["proj4"]
    #     self.assertTrue(proj4)

    # def test_get_scene_ls7(self):
    #     imagery = Imagery()
    #     scene_id = "LE70410352011200EDC00"
    #     bqa = imagery.get_band(scene_id, "BQA")
    #     proj4 = bqa["proj4"]
    #     self.assertTrue(proj4)


if __name__ == "__main__":
    unittest.main()
