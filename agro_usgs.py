import glob
import logging
import os
import re
import requests
import shutil
import tarfile
import tempfile
import time
import usgs_api
from utils import date_fmt, Utils
from gdal_utils import gdalinfo


class UsgsBackend(Utils):
    types = {
        "LS8": ["LANDSAT_8_C1", "EE"],
        "LS7": ["LANDSAT_ETM_C1", "EE"],
        "LS5": ["LANDSAT_TM_C1", "EE"],
    }

    def __init__(self, imagery_cache_dir="/reset/", log_file=None):
        self._api_key = None
        self._imagery_cache_dir = imagery_cache_dir
        self._log_file = log_file

    def debug(self, msg):
        if self._log_file:
            self._log_file.write(msg + "\n")

    @property
    def api_key(self):
        if not self._api_key or not self._api_key["data"]:
            start = time.process_time()
            self._api_key = usgs_api.login("idsgroup", "Bazezo99#")
            logging.getLogger("er2").debug(
                "Login took {0} seconds.".format(time.process_time() - start)
            )
        return self._api_key["data"]

    @staticmethod
    def return_imagery(search):
        items = search["data"]["results"]
        ret = []
        for d in items:
            # Add scene band info
            d["id"] = d["entityId"]
            d["datetime"] = d["publishDate"]
            d["thumbnail"] = {
                # LandsatLook Natural Color Preview Image
                "href": d["browse"][0]["browsePath"],
            }
            # load metadata
            metadata = d["metadata"]
            for item in metadata:
                k, v = item["fieldName"], item["value"]
                d[k] = v

            d["eo:cloud_cover"] = float(d.get("cloudCover", -1))
            if d["eo:cloud_cover"] > -1:
                ret.append(d)
        return ret

    def imagery(self, ls_type, centroid, start_date, end_date):
        dataset, node = self.types[ls_type]
        ret = []
        if self.api_key:
            search = usgs_api.search(
                dataset=dataset,
                node=node,
                lng=centroid[0],
                lat=centroid[1],
                start_date=start_date.strftime(date_fmt),
                end_date=end_date.strftime(date_fmt),
                api_key=self.api_key,
            )
            ret = self.return_imagery(search)
        return ret

    def imagery_row_path(self, ls_type, row, path, start_date, end_date):
        dataset, node = self.types[ls_type]
        # http://kapadia.github.io/usgs/reference/api.html
        # results = usgs_api.dataset_fields(dataset, node, api_key=self.api_key)
        # doesn't seem to be possible to specify row and path
        # path_field, row_field = None, None
        # for field in results["data"]:
        #     if field["name"] == "WRS Path":
        #         path_field = field["fieldId"]
        #     if field["name"] == "WRS Row":
        #         row_field = field["fieldId"]

        # assert path_field, "Could not field ID for path"
        # assert row_field, "Could not field ID for row"
        # where = {
        #     path_field: str(path),
        #     row_field: str(row),
        # }

        search = usgs_api.search(
            dataset,
            node,
            start_date=start_date.strftime(date_fmt),
            end_date=end_date.strftime(date_fmt),
            api_key=self.api_key,
        )
        return self.return_imagery(search)

    def get_metadata(self, scene_id):
        metadata = {}
        # remove the LGN00
        file_path = os.path.join(self._imagery_cache_dir, scene_id[:-5] + "_MTL.txt")
        with open(file_path) as fp:
            for r in fp:
                if "=" in r:
                    k, v = [t.strip() for t in r.split("=")]
                    metadata[k] = v
        return metadata

    def download_scene(self, scene):
        """Download to cache"""
        scene_id = scene["id"]
        # remove the LGN00
        test_path = os.path.join(self._imagery_cache_dir, scene_id[:-5] + "_B1.TIF")
        ret = []

        # Check cache
        if "LE8" in scene_id or "LC8" in scene_id:
            ls_type = "LS8"
        elif "LE7" in scene_id:
            ls_type = "LS7"
        elif "LT5" in scene_id:
            ls_type = "LS5"
        else:
            ls_type = None
        if ls_type:
            dataset, node = self.types[ls_type]
            if not os.path.exists(test_path):
                label = f"agroet_{scene['id']}"
                ret = usgs_api.download(
                    dataset, node, scene, label=label, api_key=self.api_key
                )
                if not ret["errorMessage"]:
                    path = os.path.join(self._imagery_cache_dir, scene_id)
                    print(f"Downloading to {path}")
                    for result in ret["data"]["availableDownloads"]:
                        print(f"Get download url: {result['url']}\n")
                        downloadFile(result["url"], path)
                    scene_downloads = usgs_api.download_prepared(
                        ret["data"]["preparingDownloads"],
                        label=label,
                        api_key=self.api_key,
                    )
                    for url in scene_downloads:
                        print(f"Scene download url: {url}\n")
                        downloadFile(url, path, self._imagery_cache_dir)

                    # url = ret["data"][0]["url"]
                    # # This is a tar file
                    # basename = os.path.basename(url)
                    # s = basename.find("?")
                    # scene_tar_path = os.path.join(path, basename[:s])
                    # if not os.path.exists(scene_tar_path):
                    #     with requests.get(url, stream=True) as r:
                    #         r.raise_for_status()
                    #         if not os.path.exists(path):
                    #             os.makedirs(path)
                    #         with open(scene_tar_path, "wb") as f:
                    #             for chunk in r.iter_content(chunk_size=8192):
                    #                 if chunk:  # filter out keep-alive new chunks
                    #                     f.write(chunk)
                    # with tarfile.open(scene_tar_path) as tf:
                    #     # Extract and rename to convention used by sat-search
                    #     tdir = tempfile.mkdtemp("ee")
                    #     tf.extractall(tdir)
                    #     for name in glob.glob(tdir + "/*"):
                    #         if not os.path.isdir(name):
                    #             self.copy_to_cache(
                    #                 self._imagery_cache_dir, name, scene_id
                    #             )
                    #     tdir_gap = os.path.join(tdir, "gap_mask")
                    #     if os.path.exists(tdir_gap):
                    #         targetpath = os.path.join(
                    #             self._imagery_cache_dir, "gap_mask"
                    #         )
                    #         if not os.path.exists(targetpath):
                    #             os.makedirs(targetpath)
                    #         for name in glob.glob(tdir_gap + "/*"):
                    #             try:
                    #                 self.copy_to_cache(targetpath, name, scene_id)
                    #             except:
                    #                 # already exists
                    #                 raise
                    #     shutil.rmtree(tdir)
                else:
                    raise Exception(ret["data"]["error"])
            self.debug("USGS download complete")
        return ret

    @staticmethod
    def copy_to_cache(tdir, name, scene_id):
        # copy original to new file
        iband = name.rfind("_B")
        if iband < 0:
            # try MTL
            iband = name.rfind("_MTL")
        band = name[iband:]
        # strip off the trailing 'LGN00'
        targetpath = os.path.join(tdir, scene_id[:-5] + band)
        shutil.move(name, targetpath)

    def get_band(self, scene, band):
        self.download_scene(scene)
        band_path = self.get_band_path(band, scene["displayId"])
        band = gdalinfo(band_path)
        return band


def downloadFile(url, path, dest):
    try:
        response = requests.get(url, stream=True)
        disposition = response.headers["content-disposition"]
        filename = re.findall("filename=(.+)", disposition)[0].strip('"')
        if not os.path.exists(path + filename):
            print(f"Downloading {filename} to {path + filename}\n")
            open(path + filename, "wb").write(response.content)
            print(f"Downloaded {filename}\n")
        if ".tar.gz" in filename:
            # now extract
            tf = tarfile.open(path + filename)
            tf.extractall(dest)

    except Exception as e:
        print(f"Failed to download from {url}. Will try to re-download.")
