import json
import logging
import os
import subprocess
from osgeo import gdal
from osgeo.gdalconst import *
import struct
import math
from pyproj import Transformer
import shutil
import tempfile

# Requires gdal
# https://stackoverflow.com/questions/37294127/python-gdal-2-1-installation-on-ubuntu-16-04


def gdalinfo(raster_path):
    if not os.path.exists(raster_path):
        raise Exception('Gdalinfo got non-existent raster {0}'.format(raster_path))
    args = ['gdalinfo', '-json', '-mm', '-proj4', '-stats', raster_path]
    try:
        output = subprocess.check_output(args)
    except subprocess.CalledProcessError:
        print(' '.join(args))
        raise

    j = json.loads(output.decode("utf-8").replace('nan', 'null'))
    x_min, y_max = j['cornerCoordinates']['upperLeft']
    x_max, y_min = j['cornerCoordinates']['lowerRight']

    # Sometimes during gdalwarp the raster can get flipped. Make sure that the extent values are sane
    #   or Openlayers will throw an error.
    if x_min > x_max:
        t = x_min
        x_min = x_max
        x_max = t
    if y_min > y_max:
        t = y_min
        y_min = y_max
        y_max = t

    r_min, r_max = j['bands'][0].get('minimum', None), j['bands'][0].get('maximum', None)
    x_res, y_res = abs(j['geoTransform'][1]), abs(j['geoTransform'][5])

    proj4 = j['coordinateSystem']['proj4']
    datatype = j['bands'][0]['type']

    ret = {
        'raster_min': r_min, 'raster_max': r_max,
        'x_res': x_res, 'y_res': y_res,
        'x_size': j['size'][0], 'y_size': j['size'][1],
        'x_min': x_min, 'x_max': x_max, 'y_min': y_min, 'y_max': y_max,
        'proj4': proj4,
        'datatype': datatype,
        'geoTransform': j['geoTransform'],
    }
    ex = get_extent(ret)
    if ex:
        ret.update(ex)
    return ret


def get_projection(raster_path):
    ret = gdalinfo(raster_path)
    return ret['proj4']


def get_extent(gdal_info_data):
    if gdal_info_data['proj4']:
        # Reproject to 3857 and 4326
        min_3857 = reproject(gdal_info_data['proj4'], 'epsg:3857', gdal_info_data['x_min'], gdal_info_data['y_min'])
        max_3857 = reproject(gdal_info_data['proj4'], 'epsg:3857', gdal_info_data['x_max'], gdal_info_data['y_max'])
        min_4326 = reproject(gdal_info_data['proj4'], 'epsg:4326', gdal_info_data['x_min'], gdal_info_data['y_min'])
        max_4326 = reproject(gdal_info_data['proj4'], 'epsg:4326', gdal_info_data['x_max'], gdal_info_data['y_max'])
        return {
            'bbox_3857': [min_3857[0], min_3857[1], max_3857[0], max_3857[1]],
            'bbox_4326': [min_4326[0], min_4326[1], max_4326[0], max_4326[1]],
        }
    return None


def reproject(s_srs, t_srs, x, y):
    """
    Reproject the x, y coordinates
    :param x: x coord
    :param y: y coord
    :param s_srs: current projection in proj4 string format
    :param t_srs: projection to reproject to
    :return: x, y
    """
    transformer = Transformer.from_crs(s_srs, t_srs)
    t_x, t_y = transformer.transform(x, y)
    return t_x, t_y


# returns the cellvalue on the band at (x, y)
def cell_value(dataset, band, x, y):
    # John Cartwright <John.C.Cartwright at noaa.gov> sent this via e-mail
    # 3/23/2006 14:17
    # He modified the PointValue routine that I had sent him
    # print 'point (real-world coords): ',x,',',y
    band = dataset.GetRasterBand(band)
    if band:
        # set a default NDV if none specified
        if band.GetNoDataValue() is None:
            ndv = -9999
            band.SetNoDataValue(ndv)
        ndv = band.GetNoDataValue()

        cols = band.XSize
        rows = band.YSize

        geotransform = dataset.GetGeoTransform()
        cell_size_x = geotransform[1]
        # Y-cell resolution is reported as negative
        cell_size_y = -1 * geotransform[5]

        minx = geotransform[0]
        maxy = geotransform[3]
        maxx = minx + (cols * cell_size_x)
        miny = maxy - (rows * cell_size_y)
        if (x < minx) or (x > maxx) or (y < miny) or (y > maxy):
            print('given point does not fall within grid')
            return None

        # calc point location in pixels
        x_loc = (x - minx) / cell_size_x
        x_loc = int(x_loc)
        y_loc = (maxy - y) / cell_size_y
        y_loc = int(y_loc)

        str_raster = band.ReadRaster(x_loc, y_loc, 1, 1, 1, 1, band.DataType)
        if str_raster:
            s_dt = gdal.GetDataTypeName(band.DataType)
            if s_dt == 'Int16':
                dbl_value = struct.unpack('h', str_raster)
            elif s_dt == 'UInt16':
                dbl_value = struct.unpack('H', str_raster)
            elif s_dt == 'Int32':
                dbl_value = struct.unpack('i', str_raster)
            elif s_dt == 'UInt32':
                dbl_value = struct.unpack('I', str_raster)
            elif s_dt == 'Float32':
                dbl_value = struct.unpack('f', str_raster)
            elif s_dt == 'Float64':
                dbl_value = struct.unpack('d', str_raster)
            elif s_dt == 'Byte':
                dbl_value = struct.unpack('B', str_raster)
            else:
                print('unrecognized DataType:', gdal.GetDataTypeName(band.DataType))
                return ndv
        else:
            # Corrupted raster?
            return ndv

        val = dbl_value[0]
        if val is None or math.isnan(val) or abs(val - ndv) < 0.000001:
            val = None
        return val

    return None


def get_default_histogram(rasterpath):
    """

    :param rasterpath:
    :return:
    """
    dataset = gdal.Open(rasterpath, GA_ReadOnly)
    hist = dataset.GetRasterBand(1).GetDefaultHistogram(force=True)
    if hist is not None:
        df_min = hist[0]
        df_max = hist[1]
        n_bucket_count = hist[2]
        pan_histogram = hist[3]
        size = (df_max - df_min) / n_bucket_count
        bucket_range = [df_min, ] + [df_min + (i + 1) * size for i in range(n_bucket_count)]
        return pan_histogram, bucket_range
    return None


def get_histogram(rasterpath, r_min=None, r_max=None, nbuckets=100):
    dataset = gdal.Open(rasterpath, GA_ReadOnly)
    val_list = dataset.GetRasterBand(1).GetHistogram(r_min, r_max, nbuckets)

    size = (r_max - r_min) / nbuckets
    bucket_range = [r_min] + [r_min + (i+1)*size for i in range(nbuckets)]
    return val_list, bucket_range


def gdal_grid(method, extent, cell_size, input_path, output_path, **kwargs):
    """
    Interpolate using gdal_grid program.
    :param method: gdal_grid method type
    :param extent: [minx, miny, maxx, maxy]
    :param cell_size: number
    :param input_path: layer.Vector
    :param output_path: destination path
    :param kwargs: gdal_grid arguments
    :return:
    """
    l, b, r, t = extent
    dx = abs(l - r)
    dy = abs(t - b)
    factor = cell_size
    size_x = int(dx / factor)
    size_y = int(dy / factor)

    if method == 'nearest':
        settings = {
            'model': 'nearest',
            'radius1': kwargs.get('radius1', 0),
            'radius2': kwargs.get('radius2', 0),
            'angle': kwargs.get('angle', 0),
            'nodata': kwargs.get('nodata', 0),
        }
        model = '{model}:radius1={radius1}:radius2={radius2}:angle={angle}:nodata={nodata}' \
            .format(**settings)
    else:
        settings = {
            'model': 'invdist',
            'power': kwargs.get('power', 2),
            'max_points': kwargs.get('max_points', 12),
            'smoothing': kwargs.get('smoothing', 0),
            'radius1': kwargs.get('radius1', 0),
            'radius2': kwargs.get('radius2', 0),
            'angle': kwargs.get('angle', 0),
            'nodata': kwargs.get('nodata', 0),
        }
        model = '{model}:power={power}:max_points={max_points}:smoothing={smoothing}:' \
                'radius1={radius1}:radius2={radius2}:angle={angle}:nodata={nodata}' \
            .format(**settings)
    args = [
        'gdal_grid',
        '-zfield', kwargs['column'],
        '-a', model,
        '-outsize', str(size_x), str(size_y),
        '-txe', str(l), str(r),
        '-tye', str(b), str(t),
        input_path,
        output_path,
    ]
    output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
    logging.getLogger('er2').debug('gdal_grid args: {0}'.format(' '.join(args)))
    logging.getLogger('er2').debug('Result of gdal_grid: {0}'.format(output))
    return {'raster': output_path, 'settings': settings}


def clip_raster_to_vector(raster_path, vector_path, output_path):
    if os.path.exists(output_path):
        os.unlink(output_path)
    args = ['gdalwarp', '-crop_to_cutline', '-co', 'compress=LZW', '-of', 'GTiff', '-cutline',
            vector_path, raster_path, output_path]
    output = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()[0]
    logging.getLogger('er2').debug('Result of clip_raster_to_vector: {0}'.format(output))
    return output_path


def reproject_raster(raster_path, src_proj4, target_proj4, cell_size=None):
    fd, tfile = tempfile.mkstemp('.tif')
    os.close(fd)
    args = ['gdalwarp', '-s_srs', src_proj4, '-t_srs', target_proj4]
    if cell_size:
        xres, yres = str(cell_size[0]), str(cell_size[1])
        args += ['-tr', xres, yres]
    args += [raster_path, tfile]
    logging.getLogger('er2').debug('Reprojecting using arguments {0}'.format(' '.join(args)))
    proc = subprocess.Popen(args, stdout=subprocess.PIPE)
    output = str(proc.communicate()[0])
    logging.getLogger('er2').debug(output)
    shutil.copyfile(tfile, raster_path)
    os.unlink(tfile)
    return


class CorruptedRasterException(Exception):
    pass
